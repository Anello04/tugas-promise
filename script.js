const searchButton = document.querySelector('.search-button');
searchButton.addEventListener('click', function () {

  const inputKeyword = document.querySelector('.input-keyword')

  fetch('http://www.omdbapi.com/?apikey=f33d91af&s=' + inputKeyword.value)
    .then(response => response.json())
    .then(response => {

      const film = response.Search;
      let cards = '';

      film.forEach(m => cards += showCards(m))

      const filmContainer = document.querySelector('.film-container');
      filmContainer.innerHTML = cards;

    });
});
function showCards(f) {
  return `<div class="col-md-4 my-3">
          <div class="card">
            <img src="${f.Poster}" class="card-img-top"/>
            <div class="card-body">
              <h5 class="card-title mb-4">${f.Title}</h5>
              <p class="card-subtitle mb-3 mt-2 text-muted">${f.Year}</p>
              <a href="#" style="text-decoration: none;">Type: ${f.Type}</a>
            </div>
          </div>
        </div>`
}